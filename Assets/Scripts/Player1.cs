﻿using UnityEngine;
using System.Collections;

public class Player1 : MonoBehaviour {
	
	public float v = 10;
	
	//Player Input
	void FixedUpdate () {
	if(Input.GetButton ("UP1"))
		{
			transform.Translate(new Vector3(0,v,0) * Time.deltaTime);
		}
		
	if(Input.GetButton ("DOWN1"))
	{
		transform.Translate(new Vector3(0,-v,0) * Time.deltaTime);
	}
	
	//Bounds
	if(transform.position.y > 12.5)
	{
		transform.position = new Vector3(transform.position.x, (float) 12.5, transform.position.z);	
	}
	if(transform.position.y < -13)
	{
		transform.position = new Vector3(transform.position.x, -13, transform.position.z);	
	}
		
	}
}
