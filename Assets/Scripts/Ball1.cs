﻿using UnityEngine;
using System.Collections;

public class Ball1 : MonoBehaviour {
	
	public float speed = 20;
	public float smoothing = 25;
	public AudioClip collideSound;
	
	//Scores
	public int player1Score = 0;
	public int player2Score = 0;
	public static Object obj = null;
	public static Object obj2 = null;
	public static Object obj3 = null;
	public static Object obj4 = null;
	private Quaternion rotations;
	
	// Use this for initialization
	void Start () {
		rotations = GameObject.FindGameObjectWithTag("SB2").transform.rotation;
		StartCoroutine("DelayStart");
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		Vector3 cvel = rigidbody.velocity;
		Vector3 tvel = cvel.normalized * speed;
		rigidbody.velocity = Vector3.Lerp(cvel, tvel, Time.deltaTime * smoothing);

		//make sure speed is never fast enough to escape boundaries
		if(speed > 50)
		{
			speed = 20;
		}
	
		//check left bound
		if(transform.position.x < -25)
		{
			player2Score++;
			transform.position = new Vector3(0, 0, transform.position.z);
			rigidbody.velocity = new Vector3(0,0,0);
			checkScore();
			StartCoroutine("DelayStart");
			
		}//Check right bound
		else if(transform.position.x > 25)
		{
			player1Score++;
			transform.position = new Vector3(0,0,transform.position.z);
			rigidbody.velocity = new Vector3(0,0,0);
			checkScore();
			StartCoroutine("DelayStart");
		}
		if(player1Score > 9)
		{
			player1Score = 0;
			player2Score = 0;
			Application.LoadLevel("Win");	
		}
	}
	
	void OnCollisionEnter (Collision obj) {
		AudioSource.PlayClipAtPoint(collideSound, obj.contacts[0].point);
	}
	
	IEnumerator DelayStart() {
		yield return new WaitForSeconds( 1 );
		rigidbody.AddForce(20,5,0);
	}
	
	void checkScore()
	{
		Destroy(obj);
		Destroy(obj2);
		Destroy(obj3);
		Destroy(obj4);
	switch(player1Score%10)
		{	
		case 0:
			break;
		case 1:
			obj2 = Instantiate(GameObject.FindGameObjectWithTag("1"), new Vector3(-5,12,(float) 7.9), rotations);
			break;
		case 2:
			obj = Instantiate(GameObject.FindGameObjectWithTag("2"), new Vector3(-5,12,(float) 7.9), rotations);
			break;
		case 3:
			obj2 = Instantiate(GameObject.FindGameObjectWithTag("3"), new Vector3(-5,12,(float) 7.9), rotations);
			break;
		case 4:
			obj = Instantiate(GameObject.FindGameObjectWithTag("4"), new Vector3(-5,12,(float) 7.9), rotations);
			break;
		case 5:
			obj2 = Instantiate(GameObject.FindGameObjectWithTag("5"), new Vector3(-5,12,(float) 7.9), rotations);
			break;
		case 6:
			obj = Instantiate(GameObject.FindGameObjectWithTag("6"), new Vector3(-5,12,(float) 7.9), rotations);
			break;
		case 7:
			obj2 = Instantiate(GameObject.FindGameObjectWithTag("7"), new Vector3(-5,12,(float) 7.9), rotations);
			break;
		case 8:
			obj = Instantiate(GameObject.FindGameObjectWithTag("8"), new Vector3(-5,12,(float) 7.9), rotations);
			break;
		case 9:
			obj2 = Instantiate(GameObject.FindGameObjectWithTag("9"), new Vector3(-5,12,(float) 7.9), rotations);
			break;
		}
		
		switch(player2Score%10)
		{	
		case 0:
			break;
		case 1:
			obj4 = Instantiate(GameObject.FindGameObjectWithTag("1"), new Vector3(19,12,(float) 7.9), rotations);
			break;
		case 2:
			obj3 = Instantiate(GameObject.FindGameObjectWithTag("2"), new Vector3(19,12,(float) 7.9), rotations);
			break;
		case 3:
			obj4 = Instantiate(GameObject.FindGameObjectWithTag("3"), new Vector3(19,12,(float) 7.9), rotations);
			
			break;
		case 4:
			obj3 = Instantiate(GameObject.FindGameObjectWithTag("4"), new Vector3(19,12,(float) 7.9), rotations);
			
			break;
		case 5:
			obj4 = Instantiate(GameObject.FindGameObjectWithTag("5"), new Vector3(19,12,(float) 7.9), rotations);
			
			break;
		case 6:
			obj3 = Instantiate(GameObject.FindGameObjectWithTag("6"), new Vector3(19,12,(float) 7.9), rotations);
			
			break;
		case 7:
			obj4 = Instantiate(GameObject.FindGameObjectWithTag("7"), new Vector3(19,12,(float) 7.9), rotations);
			
			break;
		case 8:
			obj3 = Instantiate(GameObject.FindGameObjectWithTag("8"), new Vector3(19,12,(float) 7.9), rotations);
			
			break;
		case 9:
			obj4 = Instantiate(GameObject.FindGameObjectWithTag("9"), new Vector3(19,12,(float) 7.9), rotations);
			
			break;
		}
	}
	
}
